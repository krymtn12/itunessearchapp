//
//  ViewController.swift
//  iTunesSearchApp
//
//  Created by Koray Metin on 22.08.2022.
//

import UIKit

class SearchViewController: UIViewController {
    var searchView: SearchViewv2 = {
        let searchView = SearchViewv2.init(frame: .zero)
        return searchView
    }()

    lazy var searchViewModel: SearchViewModel = SearchViewModel()
    var timer: Timer?
    var searchText: String?
    let overviewDataSource = OverviewDataSource()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationItem.title = "Search"
        view.addSubview(searchView)
        searchView.fillSuperview()
        searchView.searchBar.delegate = self

        self.searchViewModel.state.bind { state in
            switch state {
            case let state as SearchLoading:
                self.searchView.loadingLabel.text = "searching for \(state.searchText)..."
                self.searchView.loadingStackView.isHidden = false
                break
            case let state as SearchLoaded:
                self.searchView.loadingStackView.isHidden = true
                self.searchView.segmentContainer.isHidden = false
                self.overviewDataSource.dataSet = state.results
                self.searchView.tableView.dataSource = self.overviewDataSource
                self.searchView.tableView.reloadData()
                break
            default:
                print("default")
                break
            }
        }
    }
}

extension SearchViewController : UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        timer?.invalidate()  // Cancel any previous timer

        if searchText.isEmpty {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.searchViewModel.cancelSearch()
                searchBar.resignFirstResponder()
            }
        }

        if searchText.count >= 3 {
            // …schedule a timer for 0.5 seconds
            timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(SearchView.performSearch(sender:)), userInfo: ["text": searchText], repeats: false)
        }
    }


    @objc func performSearch(sender: Timer) {
        guard let userInfo = sender.userInfo as? [String: String],
              let searchText = userInfo["text"] else {
            return
        }
        searchViewModel.searchEvent(searchText: searchText)
    }
}

