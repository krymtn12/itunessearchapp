//
//  SearchStates.swift
//  iTunesSearchApp
//
//  Created by Koray Metin on 7.11.2022.
//

import Foundation


class OverviewSearchState {}

class SearchUnitializeState: OverviewSearchState {}
class SearchLoading: OverviewSearchState {
    var searchText: String
    init(searchText: String) {
        self.searchText = searchText
    }
}
class SearchLoaded: OverviewSearchState {
    var descText: String?
    var results: [String : Any]
    init(values: [String : Any], resultText: String) {
        self.results = values
        self.descText = resultText
    }
}
class ResultsEmptyState: OverviewSearchState {}
class SearchErrorState: OverviewSearchState {}
