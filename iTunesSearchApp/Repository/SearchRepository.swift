//
//  SearchRepository.swift
//  iTunesSearchApp
//
//  Created by Koray Metin on 4.09.2022.
//

import Foundation

class iTunesSearchRepository {

    let baseUrl = "https://itunes.apple.com/search"
    private let urlSession: URLSession

    init(urlSession: URLSession = .shared) {
        self.urlSession = urlSession
    }

    /*
    func search(with text: String, handler: @escaping (Result<[SearchResult], Error>) -> Void) {
        let path = "?term=\(text)"
        if let requestURL = URL(string: baseUrl+path) {
            do {
              let searchRequest = try URLRequest.init(url: requestURL, body: nil, method: .GET)
                self.urlSession.execute(urlRequest: searchRequest, model: SearchResult.ResultData.self) { result in
                    switch result {
                    case .success(let resultData):
                        handler(.success(resultData.results))
                    case .failure(let error):
                        handler(.failure(error))
                    }
                }
            } catch {

            }
        }
    }*/
}
