//
//  Album.swift
//  iTunesSearchApp
//
//  Created by Koray Metin on 4.11.2022.
//

import Foundation

struct Album: Codable {
    var artistName: String?
    var collectionName: String?
    var artworkUrl60: String?
    var artworkUrl100: String?
}
