//
//  Song.swift
//  iTunesSearchApp
//
//  Created by Koray Metin on 2.11.2022.
//

import Foundation

struct Song: Codable {
    var artistId: Int?
    var trackId: Int?
    var trackName: String?
    var artistName: String?
    var collectionName: String?
    var artworkUrl100: String?
    var trackPrice: Double?
}
