//
//  MusicVideo.swift
//  iTunesSearchApp
//
//  Created by Koray Metin on 5.11.2022.
//

import Foundation

struct MusicVideo: Codable {
    var artistName: String?
    var trackName: String?
    var artworkUrl60: String?
    var artworkUrl100: String?
    var previewUrl: String?
}
