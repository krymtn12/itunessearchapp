//
//  Track.swift
//  iTunesSearchApp
//
//  Created by Koray Metin on 22.08.2022.
//

import Foundation



struct SearchResponse<T: Codable>: Codable {
    var results: [T]
    var resultCount: Int?
}




class SearchResult: Codable {
    var wrapperType: String?
    var kind: String?
}

extension SearchResult {
    struct ResultData: Codable {
        var results: [SearchResult]
        var resultCount: Int?
    }
    /*
    struct ResultData: Codable {
        struct Container: Codable {
            var trackId: Int?
            var name: String?
        }
        var result: [Container]
    }*/
}


