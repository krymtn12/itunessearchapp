//
//  APITarget.swift
//  iTunesSearchApp
//
//  Created by Koray Metin on 22.08.2022.
//

import Foundation

enum HTTPMethod : String {
    case GET = "GET"
    case POST = "POST"
    case PUT = "PUT"
    case PATCH = "PATCH"
    case DELETE = "DELETE"
}

enum DataError: Error {
    case invalidURL
    case invalidResponse
    case invalidData
    case decodingError
    case serverError
}

protocol APIProtocol {
    var endPoint: String { get }

}

extension URLRequest {
    init(url: URL, body: [String: Any?]?, method: HTTPMethod) throws {
        self.init(url: url)
        httpMethod = method.rawValue
        switch method {
        case .GET, .DELETE:
            allHTTPHeaderFields = ["Accept": "application/json"]
        case .POST, .PUT, .PATCH:
            allHTTPHeaderFields = ["Accept": "application/json", "Content-Type": "application/json"]
            do {
                httpBody = try JSONSerialization.data(withJSONObject: body!, options: .prettyPrinted)
            } catch {
                print(error.localizedDescription)
            }
        }
    }
}


extension URLSession {
    func execute<T: Codable>(urlRequest: URLRequest, model:T.Type, result: @escaping (Result<T, Error>) -> Void) {

        let task = dataTask(with: urlRequest, completionHandler: { data, response, error in
            if let error = error {
                result(.failure(error))
            }

            guard let response = response as? HTTPURLResponse else {
                result(.failure(DataError.invalidResponse))
                return
            }

            if 200 ... 299 ~= response.statusCode {
                if let data = data {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                        print(json)
                    } catch {
                        print("Something went wrong")
                    }
                    do {
                        let decodedData: T = try JSONDecoder().decode(T.self, from: data)
                        result(.success(decodedData))
                    }
                    catch {
                        result(.failure(DataError.decodingError))
                    }
                } else {
                    result(.failure(DataError.invalidData))
                }
            } else {
                result(.failure(DataError.serverError))
            }
        })
        task.resume()
    }
}


