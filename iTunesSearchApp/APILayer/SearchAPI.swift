//
//  SearchAPI.swift
//  iTunesSearchApp
//
//  Created by Koray Metin on 31.10.2022.
//

import Foundation
import UIKit

protocol SearchAPITargets {
    func search<T>(with text: String, entity: String?, limit: Int?, parser: T.Type,  handler: @escaping (Result<[T], Error>) -> Void) where T: Codable
   
}

class SearchAPI: APIProtocol, SearchAPITargets {

    var endPoint: String {
        return "https://itunes.apple.com/search"
    }
    private let urlSession: URLSession

    init(urlSession: URLSession = .shared) {
        self.urlSession = urlSession
    }

    /*/
    func search<T: Codable>(with text: String, handler: @escaping (Result<[T], Error>) -> Void) {
        let path = "?term=\(text)"
        if let requestURL = URL(string: endPoint+path) {
            do {
              let searchRequest = try URLRequest.init(url: requestURL, body: nil, method: .GET)
                self.urlSession.execute(urlRequest: searchRequest, model: ResultData<T>.self) { result in
                    switch result {
                    case .success(let resultData):
                        handler(.success(resultData.results))
                    case .failure(let error):
                        handler(.failure(error))
                    }
                }
            } catch {

            }
        }
    }*/

    func setSearchPath(text: String, entity: String?, limit: Int?) -> String {
        var path = "?term=\(text)"
        if let limit { path+="&limit=\(limit)"}
        if let entity { path+="&entity=\(entity)"}

        return path
    }


    func search<T: Codable>(with text: String, entity: String?, limit: Int?, parser: T.Type, handler: @escaping (Result<[T], Error>) -> Void) {
        let path = setSearchPath(text: text, entity: entity, limit: limit)
        if let requestURL = URL(string: (endPoint+path).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
            do {
              let searchRequest = try URLRequest.init(url: requestURL, body: nil, method: .GET)
                self.urlSession.execute(urlRequest: searchRequest, model: SearchResponse<T>.self) { result in
                    switch result {
                    case .success(let resultData):
                        handler(.success(resultData.results))
                    case .failure(let error):
                        handler(.failure(error))
                    }
                }
            } catch {

            }
        } else {
            handler(.failure(DataError.invalidURL))
        }
    }

}
