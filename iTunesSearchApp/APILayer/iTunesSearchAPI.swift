//
//  iTunesSearchAPI.swift
//  iTunesSearchApp
//
//  Created by Koray Metin on 22.08.2022.
//

import Foundation


enum iTunesSearchTargets {
    case search(searchText: String)
    case trackDetail(trackId: Int)
}
protocol IHTTPService {
    var baseUrl: String { get }
    var path: String { get }
    var method: HTTPMethod { get }
    var header: [String : String] { get }
    var body: [String : Any]? { get }
    var sampleData: Data? { get }
}

extension IHTTPService {

    func request<T: Codable>(model: T.Type, completion: @escaping (Result<T, Error>) -> Void) {

        let endPoint = "\(baseUrl)\(path)"
        guard let url = URL(string: endPoint) else {
            return
        }
        var urlRequest: URLRequest = URLRequest.init(url: url)
        urlRequest.allHTTPHeaderFields = header
        switch method {
        case .GET:
            urlRequest.httpMethod = "GET"
        case .POST:
            urlRequest.httpMethod = "POST"
            //urlRequest.allHTTPHeaderFields = ["Accept": "application/json", "Content-Type": "application/json"]
            do {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: body!, options: .prettyPrinted)
            } catch {
                print(error.localizedDescription)
            }
        case .PATCH:
            urlRequest.httpMethod = "PATCH"
            //urlRequest.allHTTPHeaderFields = ["Accept": "application/json", "Content-Type": "application/json"]
            do {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: body!, options: .prettyPrinted)
            } catch {
                print(error.localizedDescription)
            }
        case .DELETE:
            urlRequest.httpMethod = "DELETE"
        case .PUT:
            break
        }

        /*
         if method == .GET {
         urlRequest.httpMethod = "GET"
         //urlRequest.allHTTPHeaderFields = ["Accept": "application/json"]
         } else if method == .POST {
         urlRequest.httpMethod = "POST"
         //urlRequest.allHTTPHeaderFields = ["Accept": "application/json", "Content-Type": "application/json"]
         do {
         urlRequest.httpBody = try JSONSerialization.data(withJSONObject: body!, options: .prettyPrinted)
         } catch {
         print(error.localizedDescription)
         }
         } else if method == .PATCH {
         urlRequest.httpMethod = "PATCH"
         //urlRequest.allHTTPHeaderFields = ["Accept": "application/json", "Content-Type": "application/json"]
         do {
         urlRequest.httpBody = try JSONSerialization.data(withJSONObject: body!, options: .prettyPrinted)
         } catch {
         print(error.localizedDescription)
         }
         } else if method == .DELETE {
         urlRequest.httpMethod = "DELETE"
         //urlRequest.allHTTPHeaderFields = ["Accept": "application/json"]
         }*/

        //create dataTask using the session object to send data to the server
        let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { data, response, error in
            if let error = error {
                completion(.failure(error))
            }

            guard let response = response as? HTTPURLResponse else {
                completion(.failure(DataError.invalidResponse))
                return
            }

            if 200 ... 299 ~= response.statusCode {
                if let data = data {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                        print(json)

                    } catch {
                        print("Something went wrong")
                    }


                    do {
                        let decodedData: T = try JSONDecoder().decode(model.self, from: data)
                        completion(.success(decodedData))
                    }
                    catch {
                        completion(.failure(DataError.decodingError))
                    }
                } else {
                    completion(.failure(DataError.invalidData))
                }
            } else {
                completion(.failure(DataError.serverError))
            }
        })
        task.resume()

    }
}

extension iTunesSearchTargets: IHTTPService {
    var sampleData: Data? {
        return nil
    }

    var method: HTTPMethod {
        switch self {
        case .search, .trackDetail:
            return .GET
        }
    }

    var baseUrl: String {
        return "https://itunes.apple.com/search"
    }

    var path: String {
        switch self {
        case .search(searchText: let text):
            return "?term=\(text)"
        case .trackDetail(trackId: let trackId):
            return "?\(trackId)"
        }
    }


    var header: [String : String] {
        return ["Accept": "application/json"]
    }

    var body: [String : Any]? {
        switch self {
        case .search, .trackDetail:
            return nil
        }
    }


}


protocol iTunesSearchTarget {
    var searchURLRequest: URLRequest { set get }
}

struct iTunesSearchAPI {

    var url: String {
        return "https://itunes.apple.com/search"
    }


}

