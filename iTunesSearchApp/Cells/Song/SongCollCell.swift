//
//  SongCollCell.swift
//  iTunesSearchApp
//
//  Created by Koray Metin on 1.11.2022.
//

import UIKit

class SongCollCell: UICollectionViewCell {

    static let cellIdentifier = "SongCollCell"

    var songs: [Song]?

    var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(SongCell.self, forCellReuseIdentifier: SongCell.cellIdentifier)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.separatorStyle = .none
        return tableView
    }()

    let tableViewDataSource = SongTableViewDataSource()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(tableView)
        tableView.anchor(top: self.topAnchor, leading: self.leadingAnchor, bottom: self.bottomAnchor, trailing: self.trailingAnchor)
    }

    func configureCell(songs: [Song]?) {
        tableViewDataSource.songs = songs
        tableView.dataSource = tableViewDataSource
        tableView.reloadData()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
