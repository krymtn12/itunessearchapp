//
//  SongCell.swift
//  iTunesSearchApp
//
//  Created by Koray Metin on 2.11.2022.
//

import UIKit

protocol SongCellRepresentable {
    var trackLabelText: String { get }
    var artistLabelText: String { get }
    var artworkUrlString: String? { get }
    var priceLabelText: String { get }
}

class SongCell: UITableViewCell {

    static let cellIdentifier = "SongCell"
    var song: Song?

    var containerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.spacing = 8
        stackView.layoutMargins = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 0)
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()

    var artworkImageView: UIImageView = {
        let imageView = UIImageView.init()
        imageView.backgroundColor = .red
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    var infoStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        return stackView
    }()

    var trackLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 13, weight: .regular)
        return label
    }()

    var artistLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .gray
        label.font = .systemFont(ofSize: 11, weight: .regular)
        return label
    }()


    var priceView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    var priceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 12, weight: .bold)
        label.textAlignment = .center
        label.textColor = .systemBlue

        label.layer.masksToBounds = true
        label.layer.borderWidth = 1
        label.layer.borderColor = UIColor.systemBlue.cgColor
        label.layer.cornerRadius = 5

        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupLayout() {
        containerStackView.addArrangedSubview(artworkImageView)
        infoStackView.addArrangedSubview(trackLabel)
        infoStackView.addArrangedSubview(artistLabel)
        containerStackView.addArrangedSubview(infoStackView)
        priceView.addSubview(priceLabel)
        priceLabel.anchor(top: priceView.topAnchor, leading: priceView.leadingAnchor, bottom: priceView.bottomAnchor, trailing: priceView.trailingAnchor, padding: .init(top: 8, left: 8, bottom: 8, right: 8))
        containerStackView.addArrangedSubview(priceView)

        self.addSubview(containerStackView)
        containerStackView.anchor(top: self.topAnchor, leading: self.leadingAnchor, bottom: self.bottomAnchor, trailing: self.trailingAnchor, size: CGSize(width: 0, height: 44))

        //artworkImageView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        artworkImageView.widthAnchor.constraint(equalToConstant: 44).isActive = true
        priceLabel.widthAnchor.constraint(greaterThanOrEqualToConstant: 64).isActive = true
        infoStackView.setContentHuggingPriority(.defaultLow, for: .horizontal)
        priceLabel.setContentHuggingPriority(.required, for: .horizontal)

    }

    func configureCell(song: Song?) {
        self.song = song
        trackLabel.text = trackLabelText
        artistLabel.text = artistLabelText
        priceLabel.text = priceLabelText
        if let urlString = artworkUrlString {
            ImageDownloader.shared.downloadImage(from: urlString, sizeOfResize: nil) { result in
                switch result {
                case .success(let image):
                    DispatchQueue.main.async {
                        self.artworkImageView.image = image
                        self.layoutIfNeeded()
                    }
                    break
                case .failure(_):
                    break
                }
            }
        }
    }

}

extension SongCell: SongCellRepresentable {

    var trackLabelText: String {
        return song?.trackName ?? "Unknown track"
    }

    var artistLabelText: String {
        return "\(song?.artistName ?? "Unknown artist") - \(song?.collectionName ?? "Unknown album") "
    }

    var artworkUrlString: String? {
        return song?.artworkUrl100
    }

    var priceLabelText: String {
        if let price = song?.trackPrice {
            return "$\(price)"
        }
        return "$0.99"
    }
}

