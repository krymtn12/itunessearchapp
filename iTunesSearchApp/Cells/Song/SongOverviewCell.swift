//
//  SongOverviewCell.swift
//  iTunesSearchApp
//
//  Created by Koray Metin on 31.10.2022.
//

import UIKit

class SongOverviewCell: OverviewCell, OverViewCellProtocol {

    var headerText: String = "Song"

    var flowLayout: UICollectionViewFlowLayout {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.itemSize = CGSize(width: 325, height: 176)
        return layout
    }

    static var cellIdentifier: String {
        return "SongOverviewCell"
    }

    var cellClass: AnyClass {
        return SongCollCell.self
    }

    let songCollectionViewDataSource = SongCollectionViewDataSource()




    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        collectionView.collectionViewLayout = flowLayout
        //collectionView.isPagingEnabled = true
        collectionView.register(SongCollCell.self, forCellWithReuseIdentifier: SongCollCell.cellIdentifier)
        collectionView.heightAnchor.constraint(equalToConstant: 176).isActive = true
        self.titleLabel.text = headerText
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configureCell(songs: [Song]?) {

        songCollectionViewDataSource.songs = songs
        collectionView.dataSource = songCollectionViewDataSource
        collectionView.reloadData()
    }


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
