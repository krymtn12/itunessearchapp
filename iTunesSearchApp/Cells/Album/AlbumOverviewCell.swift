//
//  AlbumOverviewCell.swift
//  iTunesSearchApp
//
//  Created by Koray Metin on 31.10.2022.
//

import UIKit

class AlbumOverviewCell: OverviewCell, OverViewCellProtocol {
    
    var headerText = "Albums"


    static var cellIdentifier: String {
        return "AlbumOverviewCell"
    }

    var cellClass: AnyClass {
        return AlbumCollCell.self
    }

    var flowLayout: UICollectionViewFlowLayout {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.itemSize = CGSize(width: 100, height: 137)
        return layout
    }

    let albumDataSource = AlbumDataSource()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        collectionView.collectionViewLayout = flowLayout
        collectionView.register(AlbumCollCell.self, forCellWithReuseIdentifier: AlbumCollCell.cellIdentifier)
        collectionView.heightAnchor.constraint(equalToConstant: 140).isActive = true
        self.titleLabel.text = headerText
    }


    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureCell(albums: [Album]?) {
        albumDataSource.albums = albums
        collectionView.dataSource = albumDataSource
        collectionView.reloadData()
    }

}
