//
//  AlbumCollCell.swift
//  iTunesSearchApp
//
//  Created by Koray Metin on 7.11.2022.
//

import UIKit

class AlbumCollCell: UICollectionViewCell {
    static let cellIdentifier = "AlbumCollCell"

    var containerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 1
        stackView.layoutMargins = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 0)
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()

    var artworkImageView: UIImageView = {
        let imageView = UIImageView.init()
        imageView.backgroundColor = .red
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    var albumLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 11, weight: .medium)
        return label
    }()

    var artistLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 11, weight: .medium)
        label.textColor = .gray
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupLayout() {
        containerStackView.addArrangedSubviews([artworkImageView, albumLabel, artistLabel])
        self.addSubview(containerStackView)
        containerStackView.anchor(top: self.topAnchor, leading: self.leadingAnchor, bottom: self.bottomAnchor, trailing: self.trailingAnchor)
        artworkImageView.heightAnchor.constraint(equalTo: artworkImageView.widthAnchor, multiplier: 1).isActive = true
        albumLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 20).isActive = true
        artistLabel.heightAnchor.constraint(equalToConstant: 15).isActive = true
    }

    func configureCell(album: Album?) {
        if let urlString = album?.artworkUrl100 {
            ImageDownloader.shared.downloadImage(from: urlString, sizeOfResize: nil) { result in
                switch result {
                case .success(let image):
                    DispatchQueue.main.async {
                        self.artworkImageView.image = image
                        self.layoutIfNeeded()
                    }
                    break
                case .failure(_):
                    break
                }
            }
        }

        albumLabel.text = album?.collectionName
        artistLabel.text = album?.artistName
    }
    
}
