//
//  SearchViewModel.swift
//  iTunesSearchApp
//
//  Created by Koray Metin on 26.08.2022.
//

import Foundation

protocol ISearchEvents {
    func searchEvent(searchText: String)
    func cancelSearch()
    func tapResult(resultIndex: Int)
}

class SearchViewModel : ISearchEvents {

    let searchAPI = SearchAPI()
    var state = ViewState<OverviewSearchState>(SearchUnitializeState())

    func searchEvent(searchText: String) {
        state.value = SearchLoading(searchText: searchText)
        startOverviewSearch(with: ["song","album"], searchText: searchText)
    }


    func startOverviewSearch(with entities: [String], searchText: String, limit: Int = 10) {
        if entities.count == 0 {
            return
        }

        let dispatchGroup = DispatchGroup()
        var dataSet: [String: Any] = [:]

        for entity in entities {
            dispatchGroup.enter()
            if entity == "song" {
                searchAPI.search(with: searchText, entity: "song" , limit: limit, parser: Song.self) { result in
                    switch result {
                    case .success(let songs):
                        dataSet["songs"] = songs
                        dispatchGroup.leave()
                        break
                    case .failure(_):
                        dispatchGroup.leave()
                        break
                    }
                }
            } else if entity == "album" {
                searchAPI.search(with: searchText, entity: "album" , limit: limit, parser: Album.self) { result in
                    switch result {
                    case .success(let albums):
                        dataSet["albums"] = albums
                        dispatchGroup.leave()
                        break
                    case .failure(_):
                        dispatchGroup.leave()
                        break
                    }
                }
            } else if entity == "musicVideo" {
                dispatchGroup.leave()
            }

        }

        dispatchGroup.notify(queue: .main) {
            // Do what ever you want after all calls are finished
            print("its over..")
            self.state.value = SearchLoaded(values: dataSet, resultText: "Search results are ...")
        }
    }

    func cancelSearch() {
        //state.value = .resultsCanceled
    }

    func tapResult(resultIndex: Int) {
        
    }
}
