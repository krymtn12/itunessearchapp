//
//  Box.swift
//  iTunesSearchApp
//
//  Created by Koray Metin on 29.08.2022.
//

import Foundation

class ViewState<T> {

    typealias Listener = (T) -> Void

    var listener: Listener?

    var value: T {
        didSet {
            listener?(value)
        }
    }

    init(_ value: T) {
        self.value = value
    }

    func bind(listener: Listener?) {
        self.listener = listener
        listener?(value)
    }
}
