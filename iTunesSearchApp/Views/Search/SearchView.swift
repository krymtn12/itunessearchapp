//
//  SearchView.swift
//  iTunesSearchApp
//
//  Created by Koray Metin on 26.08.2022.
//

import Foundation
import UIKit

protocol SearchViewProtocol {
    func showSearchSteps(text: String)
    func hideSearchSteps()
}

class SearchView: XIBBaseView {

    override var xibType: KXIBTypes {
        return .elastic
    }

    @IBOutlet private(set) var searchBar: UISearchBar!
    @IBOutlet private(set) var searchStepsLabel: UILabel!
    @IBOutlet private(set) var spinner: UIActivityIndicatorView!

    lazy var searchViewModel: SearchViewModel = SearchViewModel()
    var timer: Timer?
    var searchText: String?

    override func setup() {
        super.setup()
        searchBar.searchBarStyle = .minimal;
        searchBar.placeholder = "musics, movies, apps.."
        self.searchBar.delegate = self

        searchStepsLabel.isHidden = true
        spinner.hidesWhenStopped = true


        self.searchViewModel.state.bind { state in
            if state is SearchUnitializeState {
                
            }
            if state is SearchLoading {
                DispatchQueue.main.async {
                    //self.showSearchSteps(text: state.desc ?? "-")
                }
            }
            if state is SearchLoaded {

            }
        }
    }
}

extension SearchView: SearchViewProtocol {
    func hideSearchSteps() {
        self.searchStepsLabel.text = ""
        searchStepsLabel.isHidden = true
        spinner.stopAnimating()
    }


    func showSearchSteps(text: String) {
        self.searchStepsLabel.text = text
        searchStepsLabel.isHidden = false
        spinner.isHidden = false
        spinner.startAnimating()
    }
}


extension SearchView : UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        timer?.invalidate()  // Cancel any previous timer

        if searchText.isEmpty {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.searchViewModel.cancelSearch()
                    searchBar.resignFirstResponder()
                }
            }

        if searchText.count >= 3 {
            // …schedule a timer for 0.5 seconds
            timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(SearchView.performSearch(sender:)), userInfo: ["text": searchText], repeats: false)
        }
    }


    @objc func performSearch(sender: Timer) {
        guard let userInfo = sender.userInfo as? [String: String],
              let searchText = userInfo["text"] else {
            return
        }
        searchViewModel.searchEvent(searchText: searchText)
    }

}


class SearchViewv2: UIView {

    var phoneStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 1
        stackView.layoutMargins = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 0)
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()

    var searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.searchBarStyle = .minimal;
        searchBar.placeholder = "musics, movies, apps.."
        return searchBar
    }()

    var loadingStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.spacing = 12
        stackView.layoutMargins = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 0)
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    var spinner: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView.init(style: .gray)
        activityIndicator.startAnimating()
        return activityIndicator
    }()
    var loadingLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 13, weight: .regular)
        return label
    }()

    var segmentContainer: UIView = {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    var segmentedControl: UISegmentedControl = {
        let control = UISegmentedControl(items: ["All", "Songs", "Albums"])
        control.translatesAutoresizingMaskIntoConstraints = false
        control.selectedSegmentIndex = 0
        return control
    }()

    var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(SongOverviewCell.self, forCellReuseIdentifier: SongOverviewCell.cellIdentifier)
        tableView.register(AlbumOverviewCell.self, forCellReuseIdentifier: AlbumOverviewCell.cellIdentifier)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.estimatedRowHeight = 60
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        return tableView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupLayout()
    }

    func setupLayout() {
        loadingStackView.addArrangedSubviews([spinner, loadingLabel])
        segmentContainer.addSubview(segmentedControl)
        segmentContainer.heightAnchor.constraint(equalToConstant: 40).isActive = true
        segmentedControl.centerXAnchor.constraint(equalTo: segmentContainer.centerXAnchor).isActive = true
        phoneStackView.addArrangedSubviews([searchBar, loadingStackView, segmentContainer, tableView])
        loadingStackView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        loadingStackView.isHidden = true
        segmentContainer.isHidden = true
        self.addSubview(phoneStackView)
        phoneStackView.anchor(top: self.safeAreaLayoutGuide.topAnchor, leading: self.safeAreaLayoutGuide.leadingAnchor, bottom: self.safeAreaLayoutGuide.bottomAnchor, trailing: self.safeAreaLayoutGuide.trailingAnchor)
    }

    /*
    func hideSearchSteps() {
        self.searchStepsLabel.text = ""
        searchStepsLabel.isHidden = true
        spinner.stopAnimating()
    }


    func showSearchSteps(text: String) {
        self.searchStepsLabel.text = text
        searchStepsLabel.isHidden = false
        spinner.isHidden = false
        spinner.startAnimating()
    }*/
}
