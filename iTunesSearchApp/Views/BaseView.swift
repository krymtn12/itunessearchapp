//
//  BaseView.swift
//  GenericHTTPRequest
//
//  Created by Koray Metin on 1.08.2022.
//

import Foundation
import UIKit

enum KXIBTypes {
    case single
    case elastic
    case companyBased
}

protocol IXIBBaseView {
    var companyPrefix: String { get }
    var nibName: String { get }
    var xibType: KXIBTypes { get }
    func setup()
}



class XIBBaseView : UIView, IXIBBaseView {

    var companyPrefix = "AA"

    var view: UIView!
    var bgColor: UIColor? {
        return traitCollection.userInterfaceStyle == .light ? UIColor(hex: "#FAFAFAFF") :.black
    }
    var xibType: KXIBTypes {
        return .single
    }
    var nibName: String {
        if xibType == .elastic {
            return String(describing: type(of: self)) + ((UIDevice.current.userInterfaceIdiom == .phone) ?"Phone" :"Pad")
        } else if xibType == .companyBased {
            return companyPrefix + String(describing: type(of: self))
        }
        return String(describing: type(of: self))
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    func setup() {
        view = loadViewFromNib()
        view.frame = self.bounds
        if let color = bgColor {
            view.backgroundColor = color
        }
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        addSubview(view)
    }


    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if let color = bgColor {
            view.backgroundColor = color
        }
    }

}
