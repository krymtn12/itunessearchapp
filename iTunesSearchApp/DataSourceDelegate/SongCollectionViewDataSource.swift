//
//  SongCollectionViewDataSource.swift
//  iTunesSearchApp
//
//  Created by Koray Metin on 1.11.2022.
//

import UIKit



class SongCollectionViewDataSource: NSObject, UICollectionViewDataSource {

    let TABLEVIEW_ITEM_COUNT = 4

    var songs: [Song]?

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let songs = self.songs else {
            return 0
        }

        return (songs.count % 4 == 0) ?songs.count/4 :(songs.count/4) + 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = collectionView.dequeueReusableCell(withReuseIdentifier: SongCollCell.cellIdentifier, for: indexPath) as? SongCollCell

        if cell == nil {
            cell = SongCollCell()
        }

        cell!.configureCell(songs: itemsOfTableView(page: indexPath.row))
        

        return cell!
    }

    func itemsOfTableView(page: Int) -> [Song] {
        guard let songs = self.songs else {
            return []
        }

        if songs.count < 4 {
            return songs
        }

        let startIndex = page*4
        let diff = songs.count - startIndex
        if diff == 1 {
            return [songs[songs.count - 1]]
        }
        let endIndex = page*4 + ((diff > 3) ?3 :diff-1)
        return Array(songs[startIndex...endIndex])
    }


}
