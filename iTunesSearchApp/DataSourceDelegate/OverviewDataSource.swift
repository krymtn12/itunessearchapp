//
//  OverviewDataSource.swift
//  iTunesSearchApp
//
//  Created by Koray Metin on 7.11.2022.
//

import UIKit

class OverviewDataSource: NSObject, UITableViewDataSource {

    var dataSet: [String: Any] = [:]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSet.keys.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: SongOverviewCell.cellIdentifier, for: indexPath) as! SongOverviewCell
            let songs = dataSet["songs"] as? [Song]
            cell.configureCell(songs: songs)
            return cell
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: AlbumOverviewCell.cellIdentifier, for: indexPath) as! AlbumOverviewCell
            let albums = dataSet["albums"] as? [Album]
            cell.configureCell(albums: albums)
            return cell
        }

        return UITableViewCell(frame: .zero)
    }
}
