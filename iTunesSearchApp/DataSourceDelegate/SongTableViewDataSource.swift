//
//  SongTableViewDataSource.swift
//  iTunesSearchApp
//
//  Created by Koray Metin on 6.11.2022.
//

import UIKit

class SongTableViewDataSource: NSObject, UITableViewDataSource {

    var songs: [Song]?

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songs?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: SongCell.cellIdentifier, for: indexPath) as? SongCell

        if cell == nil {
            cell = SongCell(style: .default, reuseIdentifier: SongCell.cellIdentifier)
        }
        let song = songs?[indexPath.row]
        cell!.configureCell(song: song)

        return cell!
    }


}
