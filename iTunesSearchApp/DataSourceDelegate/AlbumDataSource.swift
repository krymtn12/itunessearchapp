//
//  AlbumDataSource.swift
//  iTunesSearchApp
//
//  Created by Koray Metin on 7.11.2022.
//

import UIKit

class AlbumDataSource: NSObject, UICollectionViewDataSource {

    var albums: [Album]?

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let albums = self.albums else {
            return 0
        }

        return albums.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = collectionView.dequeueReusableCell(withReuseIdentifier: AlbumCollCell.cellIdentifier, for: indexPath) as? AlbumCollCell

        if cell == nil {
            cell = AlbumCollCell()
        }

        cell!.configureCell(album: albums?[indexPath.row])


        return cell!
    }
}
