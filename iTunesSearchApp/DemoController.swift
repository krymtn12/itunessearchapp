//
//  DemoController.swift
//  iTunesSearchApp
//
//  Created by Koray Metin on 31.10.2022.
//

import UIKit

class DemoController: UIViewController {

    var results: [String: [Codable]]?

    let tableViewDataSource = SongTableViewDataSource()
    var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(SongOverviewCell.self, forCellReuseIdentifier: SongOverviewCell.cellIdentifier)
        tableView.register(AlbumOverviewCell.self, forCellReuseIdentifier: AlbumOverviewCell.cellIdentifier)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.estimatedRowHeight = 60
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        return tableView
    }()

    var songs: [Song]?
    var albums: [Album]?
    var dataSet: [String: Any] = [:]



    lazy var searchViewModel: SearchViewModel = SearchViewModel()

    var searchView: SearchViewv2 = {
        let searchView = SearchViewv2.init(frame: .zero)
        return searchView
    }()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationItem.title = "Search"
        
        view.addSubview(searchView)
        searchView.fillSuperview()

        //view.addSubview(tableView)
        //tableView.fillSuperview()
        //tableView.dataSource = tableViewDataSource
        //tableView.dataSource = self
        //overviewSearch(with: ["song","album"], searchText: "maroon5")

        self.searchViewModel.state.bind { state in
            if state is SearchUnitializeState {

            }
            if state is SearchLoading {
                DispatchQueue.main.async {
                    //self.showSearchSteps(text: state.desc ?? "-")
                }
            }
            if state is SearchLoaded {
                self.searchView.tableView.dataSource = self
                self.songs = (state as! SearchLoaded).results["songs"] as? [Song]
                self.albums = (state as! SearchLoaded).results["albums"] as? [Album]
                self.searchView.tableView.reloadData()
            }
        }
        
    }
    let api = SearchAPI()

    func overviewSearch(with entities: [String], searchText: String, limit: Int = 10) {
        if entities.count == 0 {
            return
        }

        let dispatchGroup = DispatchGroup()

        for entity in entities {
            dispatchGroup.enter()

            if entity == "song" {
                api.search(with: searchText, entity: "song" , limit: limit, parser: Song.self) { result in
                    switch result {
                    case .success(let songs):
                        self.dataSet["song"] = songs
                        dispatchGroup.leave()
                        break
                    case .failure(_):
                        dispatchGroup.leave()
                        break
                    }
                }
            } else if entity == "album" {
                api.search(with: searchText, entity: "album" , limit: limit, parser: Album.self) { result in
                    switch result {
                    case .success(let albums):
                        self.dataSet["albums"] = albums
                        dispatchGroup.leave()
                        break
                    case .failure(_):
                        dispatchGroup.leave()
                        break
                    }
                }
            } else if entity == "musicVideo" {
                dispatchGroup.leave()
            }

        }

        dispatchGroup.notify(queue: .main) {
            // Do what ever you want after all calls are finished
            print("its over..")
            //self.tableViewDataSource.songs = dataSet["song"] as? [Song]
            self.songs = self.dataSet["song"] as? [Song]
            self.albums = self.dataSet["albums"] as? [Album]
            self.tableView.reloadData()
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DemoController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSet.keys.count
    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: SongOverviewCell.cellIdentifier, for: indexPath) as! SongOverviewCell
            cell.configureCell(songs: self.songs)
            return cell
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: AlbumOverviewCell.cellIdentifier, for: indexPath) as! AlbumOverviewCell
            cell.configureCell(albums: self.albums)
            return cell
        }

        return UITableViewCell(frame: .zero)
    }


}
