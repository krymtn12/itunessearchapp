//
//  ImageDownloader.swift
//  iTunesSearchApp
//
//  Created by Koray Metin on 6.11.2022.
//

import UIKit

enum DownloadImageError: Error {
    case invalidURL
    case invalidData
}

class ImageDownloader {
    static let shared = ImageDownloader()

    var images: [String : UIImage] = [:]

    func downloadImage(from stringUrl: String, sizeOfResize: CGSize?, handler: @escaping (Result<UIImage, DownloadImageError>) -> Void) {
        if let image = images[stringUrl] {
            handler(.success(image))
            return
        }
        guard let url = URL(string: stringUrl) else {
            handler(.failure(DownloadImageError.invalidURL))
            return
        }

        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil,
                  let image = UIImage(data: data) else {
                handler(.failure(DownloadImageError.invalidData))
                return
            }
            self.images[stringUrl] = image
            handler(.success(image))
        }
        task.resume()
    }
}
