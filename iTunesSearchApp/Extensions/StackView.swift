//
//  StackViewExtensions.swift
//  AnchorMasters
//
//  Created by Koray Metin on 23.10.2022.
//

import UIKit

extension UIStackView {
    func addArrangedSubviews(_ subviews: [UIView]) {
        subviews.forEach{ self.addArrangedSubview($0) }
    }
}
