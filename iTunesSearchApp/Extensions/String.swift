//
//  StringExtension.swift
//  iTunesSearchApp
//
//  Created by Koray Metin on 22.08.2022.
//

import Foundation

extension String {
    // https://stackoverflow.com/questions/24551816/swift-encode-url
    var urlEncoded: String? {
        let allowedCharacterSet = CharacterSet.alphanumerics.union(CharacterSet(charactersIn: "~-_."))
        return self.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet)
    }
}
