//
//  api_layer_tests.swift
//  iTunesSearchAppTests
//
//  Created by Koray Metin on 22.08.2022.
//

import XCTest
@testable import iTunesSearchApp



class api_layer_tests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        let target = MockiTunesSearchTargets.search(searchText: "jazz class")
        XCTAssertEqual(target.method, .GET)


        XCTAssertNotNil(target.sampleData, "Sample data is not nil")

        let response = try! JSONDecoder().decode(Track.ResultData.self, from: target.sampleData!)

        XCTAssertEqual(response.resultCount, 50)

        XCTAssertEqual(response.results.first?.kind, WrapperKind.song)




    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}


enum MockiTunesSearchTargets {
    case search(searchText: String)
    case trackDetail(trackId: Int)
}

extension MockiTunesSearchTargets: IHTTPService {
    var baseUrl: String {
        return ""
    }

    var path: String {
        return ""
    }

    var method: HTTPMethod {
        return .GET
    }

    var header: [String : String] {
        return [:]
    }

    var body: [String : Any]? {
        return nil
    }

    var sampleData: Data? {
        switch self {
        case .search:
            return getSampleData(with: "search_sample")
        case .trackDetail:
            return nil
        }
    }


    func getSampleData(with jsonFile: String) -> Data? {
        if let path : String = Bundle(for: api_layer_tests.self).path(forResource: jsonFile, ofType: "json") {
                   if let data = try? Data(contentsOf: URL(fileURLWithPath: path)) {
                       return data
                   }
        }
        return nil
    }

}
