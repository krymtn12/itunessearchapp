//
//  search_viewmodel_tests.swift
//  iTunesSearchAppTests
//
//  Created by Koray Metin on 4.09.2022.
//

import XCTest
@testable import iTunesSearchApp

class search_viewmodel_tests: XCTestCase {

    var searchTarget: MockiTunesSearchTargets?
    var searchResponse: Track.ResultData?

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        searchTarget = MockiTunesSearchTargets.search(searchText: "jazz class")
        searchResponse = try! JSONDecoder().decode(Track.ResultData.self, from: searchTarget!.sampleData!)
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        searchTarget = nil
        searchResponse = nil
    }


    func testFilterWithKind() throws {
        let featureMoview = searchResponse!.results.filter { track in
            return track.kind == .book
        }

        XCTAssertEqual(featureMoview.count, 1)
        
    }

    func testGetUniqueKinds() throws {
        let uniqueKinds = Set(searchResponse!.results.compactMap { track in
            return track.kind
        })

        XCTAssertEqual(uniqueKinds.count, 3)

    }

    func testStateCase() throws {
        let searchVM = SearchViewModel()
        let expectation = expectation(description: "Get request completed")
        searchVM.searchEvent(searchText: "jack jhonson")
        let asyncWaitDuration = 3.5
         DispatchQueue.main.asyncAfter(deadline: .now() + asyncWaitDuration) {
           expectation.fulfill()
             let state = searchVM.state.value
             XCTAssertEqual(state, .resultsLoaded(results: []))
         }

        wait(for: [expectation], timeout: asyncWaitDuration)


    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
