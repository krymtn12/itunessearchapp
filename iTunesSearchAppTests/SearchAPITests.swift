//
//  SearchAPITests.swift
//  iTunesSearchAppTests
//
//  Created by Koray Metin on 1.11.2022.
//

import XCTest
@testable import iTunesSearchApp
final class SearchAPITests: XCTestCase {

    var mockSearchAPI: MockSearchAPI!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        mockSearchAPI = MockSearchAPI()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        mockSearchAPI = nil
    }

    func testDecodeSongModel() throws {

        var songArray = [Song]()
        let expectation = expectation(description: "parse sample json with Song model")

        mockSearchAPI.search(with: "aa", entity: nil, limit: nil, parser: Song.self, handler: { result in
                switch result {
                case .success(let songs):
                    songArray.append(contentsOf: songs)
                    expectation.fulfill()
                    break
                case .failure(_):
                    break
                }
        })

        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertEqual(songArray[2].collectionName, "Jack Johnson and Friends: Sing-A-Longs and Lullabies for the Film Curious George")
    }

    func setSearchPath(text: String, entity: String?, limit: Int?) -> String {
        var path = "?term=\(text)"
        if let limit { path+="&limit=\(limit)"}
        if let entity { path+="&entity=\(entity)"}

        return path
    }

    func testSearchPathGenerator() throws {
        let text = "Metallica"
        let entity: String? = nil
        let limit = 10

        let newPath = setSearchPath(text: text, entity: entity, limit: limit)

        XCTAssertEqual(newPath, "adasd")
    }

    func testOverviewAPI() throws {
        let api = SearchAPI()

        let expec = expectation(description: "song lenght should be 10")
        var songs :[Song] = []
        api.search(with: "metallica", entity: "song", limit: 10, parser: Song.self) { result in
            switch result {
            case .success(let items):
                songs.append(contentsOf: items)
                expec.fulfill()
                break
            case .failure(_):
                break
            }
        }

        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertEqual(songs.count, 10)
    }
    

    func testMultipleRequestForOverview() {
        // create album song musicVideo local jsons
        // https://stackoverflow.com/questions/62440313/make-multiple-network-requests-on-swift
        // ["song","album",...]


        overviewSearchTest(with: ["song","album","musicVideo"], searchText: "maroon5")
    }

    func overviewSearchTest(with entities: [String], searchText: String, limit: Int = 10) {
        if entities.count == 0 {
            return
        }

        let expSong = expectation(description: "get song datasource by search text")
        let expAlbum = expectation(description: "get album datasource by search text")
        let expMusicVideo = expectation(description: "get musicVideo datasource by search text")

        var dataSet: [String: Any] = [:]
        for entity in entities {
            if entity == "song" {
                mockSearchAPI.search(with: searchText, entity: "song" , limit: limit, parser: Song.self) { result in
                    switch result {
                    case .success(let songs):
                        dataSet["song"] = songs
                        expSong.fulfill()
                        break
                    case .failure(_):
                        break
                    }
                }
            } else if entity == "album" {
                mockSearchAPI.search(with: searchText, entity: "album" , limit: limit, parser: Album.self) { result in
                    switch result {
                    case .success(let albums):
                        dataSet["album"] = albums
                        expAlbum.fulfill()
                        break
                    case .failure(_):
                        break
                    }
                }
            } else if entity == "musicVideo" {
                mockSearchAPI.search(with: searchText, entity: "musicVideo" , limit: limit, parser: MusicVideo.self) { result in
                    switch result {
                    case .success(let videos):
                        dataSet["musicVideo"] = videos
                        expMusicVideo.fulfill()
                        break
                    case .failure(_):
                        break
                    }
                }
            }
        }


        wait(for: [expSong,expAlbum,expMusicVideo], timeout: 4.0)
        // Do what ever you want after all calls are finished
        print(dataSet)
        XCTAssertEqual((dataSet["song"] as! [Song]).first?.collectionName, "fsdfsdf")

    }

func testPerformanceExample() throws {
    // This is an example of a performance test case.
    self.measure {
        // Put the code you want to measure the time of here.
    }
}

}



class MockSearchAPI: APIProtocol, SearchAPITargets {

    var endPoint: String = ""


    func search<T: Codable>(with text: String, entity: String?, limit: Int?, parser: T.Type, handler: @escaping (Result<[T], Error>) -> Void) {

        var resource: String?
        if entity == "song" {

        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
            if let path : String = Bundle(for: MockSearchAPI.self).path(forResource: "song", ofType: "json") {
                do {
                    let data = try Data(contentsOf: URL(fileURLWithPath: path))
                    let resultData = try JSONDecoder().decode(SearchResponse<T>.self, from: data)
                    handler(.success(resultData.results))
                } catch let error {
                    handler(.failure(error))
                }
            }})
    }

}
