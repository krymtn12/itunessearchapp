//
//  OverviewCellTests.swift
//  iTunesSearchAppTests
//
//  Created by Koray Metin on 31.10.2022.
//

import XCTest
@testable import iTunesSearchApp
final class OverviewCellsTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testGenericOverviewCellInit() throws {
        let identifier = "OverviewCellIde"
        let overviewCell = OverviewCell(style: .default, reuseIdentifier: identifier)
        XCTAssertNotNil(overviewCell, "overview cell is nil")

        XCTAssertEqual(overviewCell.titleLabel.text, "bok")
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
