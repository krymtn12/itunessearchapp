//
//  SongOverviewCellTests.swift
//  iTunesSearchAppTests
//
//  Created by Koray Metin on 1.11.2022.
//

import XCTest
@testable import iTunesSearchApp

final class SongOverviewCellTests: XCTestCase {

    var songOverviewCell: SongOverviewCell!
    var songs: [Song]!
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        songOverviewCell = SongOverviewCell(style: .default, reuseIdentifier: SongOverviewCell.cellIdentifier)
        songs = [Song]()
        if let path : String = Bundle(for: MockSearchAPI.self).path(forResource: "search_sample", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path))
                let resultData = try JSONDecoder().decode(SearchResponse<Song>.self, from: data)
                songs.append(contentsOf: resultData.results)
            } catch let error {
                print(error)
            }
        }
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        songOverviewCell = nil
        songs = nil
    }

    func testInitSongOverviewCell() throws {
        XCTAssertNotNil(songOverviewCell)
        XCTAssertEqual(songOverviewCell.headerText, "Song")
        XCTAssertEqual((songOverviewCell.collectionView.collectionViewLayout as! UICollectionViewFlowLayout).scrollDirection , .horizontal)
    }

    func testSongCollectionViewDataSource() throws {
        let songCollectionViewDataSource = SongCollectionViewDataSource()
        songOverviewCell.collectionView.dataSource = songCollectionViewDataSource
        XCTAssertEqual(songOverviewCell.collectionView.numberOfSections, 1)
        XCTAssertEqual(songOverviewCell.collectionView.numberOfItems(inSection: 0), 0)
    }

    func testNumberOfItemsOfCollectionView() throws {
        // we calculate number of tableviews
        XCTAssertEqual(self.songs.count, 50)

        let songCollectionViewDataSource = SongCollectionViewDataSource()
        songCollectionViewDataSource.songs?.append(contentsOf: self.songs)

        XCTAssertEqual(songCollectionViewDataSource.TABLEVIEW_ITEM_COUNT, 4)

        let numberOfTables = calculateNumberOfItems()
        XCTAssertEqual(numberOfTables, 13)
    }

    func calculateNumberOfItems() -> Int {
        guard let songs else {
            return 0
        }

        return (songs.count % 4 == 0) ?songs.count/4 :(songs.count/4) + 1
    }

    func testTableViewDataSourceItem() throws {
        let datasource = itemsOfTableView(page: 1)
        // returns songs[4..7]
        XCTAssertEqual(datasource[2].collectionName, songs[6].collectionName)
    }

    func itemsOfTableView(page: Int) -> [Song] {
        let startIndex = page*4
        let endIndex = page*4 + 3
        return Array(songs[startIndex...endIndex])
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
