//
//  TrackCollectionTests.swift
//  iTunesSearchAppTests
//
//  Created by Koray Metin on 29.10.2022.
//

import XCTest
@testable import iTunesSearchApp

final class TrackCollectionTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testTrackCellInit() throws {
        let trackCell = TrackCollCell(frame: .zero)
        XCTAssertNotNil(trackCell, "track cell is nil")
        XCTAssertEqual(trackCell.trackLabel.text, "track")
    }

    func trackCollectionDataSourceTests() throws {
        let collectionView = UICollectionView(frame: .zero)
       
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
